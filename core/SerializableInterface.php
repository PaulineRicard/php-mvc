<?php

interface SerializableInterface
{
    
    public function serialize();
    public function unSerialize($data);

}