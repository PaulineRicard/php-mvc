<?php

abstract class EntityModel extends DAO implements PersistableInterface, SerializableInterface
{
    public $_dao;
    
    //*METHOD
    //*     //*Construc
    public function __construct()
    {
      //todo a faire !!!!!!!!!!!
    }
    
    //*     //* load(): invoque la méthode retrieve sur le dao en passant en argument l’id de l’entité courante.
    public function load($id)
    {
        $id = get_class($this);
        //*$this->_dao->retrieve($id);
        $données = $this->_dao->retrieve($id);
        return $données;
    }
    //*     //* update(): invoque la méthode create() sur le dao si l’entité courante n’as pas d’id et update dans le cas contraire.
    public function update($id)
    {
        $id = get_class($this);
        if($id):
            $this->_dao->update($id);
        else : $this->_dao->create($id);
        endif;
    }
    //*     //* remove(): invoque la méthode delete() du dao.
    public function remove($id)
    {
        $id = get_class($this);
       $this->_dao->delete($id);
    }

    public function serialize()
    {
        //return serialize($this->_dao);
    }
    public function unserialize($_dao)
    {
        //$this->_dao = unserialize($_dao);
    }
}