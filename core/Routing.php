<?php

class Routing
{
    //*     //* PRORIETES
    public $_config; //*représentation  en  tableau  associatif  du  fichier routing.json
    public $_uri; //* résultat du découpage de l’URI en tableau
    public $_route; //* résultat du découpage de la route (clé de config) testée
    public $_controller; //* correspond au contrôleur qui a été trouvé
    public $_args; //* tableau des arguments à passer au contrôleur, qui représente les éléments variables de l’URI
    public $_method; //* correspond au verbe http utilisé lors de la requete



    //*     //* METHOD
    public function __construct()
    {
        //*recupère le contenu du fichier routing.json dans la variable $json
        $json = file_get_contents('/home/pauline/ServeurWeb/php-mvc/config/ routing.json');
        //* $_config initialisée à la  création  de l’objet => tableau  associatif  du  fichier routing.json (option true de json_decode)
        $this->_config = json_decode($json, true);
        //* $_uri initialisée à la  création  de l’objet => tableau du resultat de $_SERVER['REQUEST_URI']
        $this->_uri = explode("/", $_SERVER['REQUEST_URI']);
    }
    // public function test()
    // {
    //    return $this->_config;
    // }


    
     //* execute() => invoquée à chaque requête http, cette méthode déclenche le mécanisme de routage
    
    public function execute()
    {
        //* récupère les propriétés config et uri dans des variables
        $config = $this->_config;
        //? var_dump($config);

        $uri = $this->_uri;
        //* si l'uri contient index.php, on le retire du tableau
        if ($uri[1] === 'index.php') {
            array_splice($uri, 1, 1);
        }
        //* appel la fonction sanitize pour $uri (enlève les 'vides' du tableau);
        $uri = $this->sanitize($uri);

        //*  met a jour la propriété $_uri de l'objet
        $this->_uri = $uri;
        //? var_dump($uri);

        //* défini un tableau contenant toutes les routes
        $routes = array_keys($config);
        //? var_dump($routes);

        //* pour chaque route du tableau routes ($route est une chaine de caractère)
        for ($i = 0; $i < sizeOf($routes); $i++) {
            //* on scinde $route en segments au niveau du/des "/" => $route = tableau de chaînes de caractères
            $route = explode('/', $routes[$i]);
            $route = $this->sanitize($route);
            //? var_dump($route);

            //* verifie pour chaque route si les tableaux $route et $uri sont de même longueur
            if ($this->isEqual($route) === true) :
                //? echo "tableaux égaux !!";
                //? var_dump($route);
                //* lorsqu'ils sont de même longueur, appel la method compare
                if ($this->compare($route) !== false) :
                    //* si la route est trouvée, met à jour la propriété $_route de l'objet et appel la method getVAlue();
                    //? print "route trouvée!!!!!!";
                    //? var_dump($route);
                    $this->_route = $route;
                    $this->getVAlue();
                    //* appel la method invoke()
                    $this->invoke();

                endif;
            endif;
        }
    }



    
    //* isEqual() => retourne un booléen correspondant au résultat de la comparaison des hauteurs des deux tableaux
     
    public function isEqual($route)
    {
        $uri =  $this->_uri;

        //* verifie la longueur des tableaux route et uri
        if (count($route) === count($uri)) :
            //* retourne true si ils sont de même longueur
            return true;
        endif;
    }


    //* getValue() => retourne le contrôleur correspondant à la route sélectionnée
 
    public function getVAlue()

    {   //* récupère les propriétés route et config de l'objet
        $route = $this->_route;
        $config = $this->_config;

        //* $routeString => Rassemble les éléments du tableau en une chaîne séparée par / (pour correspondre a la clé du tableau config)
        $routeString = "/".implode("/", $route);
        //* $value correspond à la valeur correspondant à la route soit c’est un tableau associatif
        $value = $config[$routeString];
        //? var_dump($routeString);
        //? var_dump($value);

        //* si $value est un string => on récupère le controller et la method directement
        if (gettype($value) === 'string') :
            $array = explode(":", $value); //* envoi dans un tableau la partie controller et la partie method (au niveau des ":")
            $this->_controller = $array; //* met à jour la propriété _controller de l'objet
            //? var_dump($this->_controller);
        //* sinon $value est un tableau et il faut vérifier le verbe http qui correspond a la clé du tableau $value
        else :
            //* recupère le verbe http dans une variable et met à jour la propriété de l'objet
            $method = $_SERVER['REQUEST_METHOD'];
            $this->_method = $method;

            //* pour chaque clé de $value on vérifie si la clé correspond au verbe http
            foreach ($value as $key => $val) {
                if ($key === $method) :
                    $array = explode(":", $val);
                    $this->_controller = $array;
                    //? var_dump($this->_controller);
                endif;
            }
        endif;
    }

    
    //* addArgument() => ajoute l’élément variable  de  l’URI  si l’élémenten cours est censé être variable

    public function addArgument($index)
    {
        $uri = $this->_uri;
        //? var_dump($uri);
        //? print($index);
        $args = [];
        //* stock la valeur de $uri[$index] (élement de variable) dans $arg et l'envoi dans le tableau $args
        $arg = $uri[$index];
        array_push($args, $arg);
        //* met a jour la propriété $_args de l'objet
        $this->_args = $args;
        var_dump($args);
    }

    //* compare() compare les éléments des deux tableaux (uri et route) si les deux tableaux correspondent, c’est que la route a été trouvée

    public function compare($route)
    {
        $uri = $this->_uri;
        //? var_dump($uri);
        //? var_dump($route);
        //* pour chaque index des tableaux route et uri
        for ($i = 0; $i < count($route); $i++) {
            //* verifie si les valeurs sont différentes
            if ($route[$i] != $uri[$i]) :
                //* si elles sont différentes, verifie si s'est un élement de variable
                if (strpos($route[$i], "(:)") !== false) :
                    $index = $i;
                    //?print $index;
                    //* appel la method addArgument
                    $this->addArgument($index);
                //*si ce n'est pas un élement de variable, retourne false
                else : return false;
                endif;
            endif;
            //* si la method ne retourne pas false, la route a été trouvée !!
        }
    }

    //* méthode invoke() invoquée quand le contrôleur a été sélectionnée. Elle créée un objet contrôleur et invoque la méthode en y passant les arguments adéquats

    public function invoke()
    {
        //*récupère la propriété _args de l'objet
        $args = $this->_args;

        //*défini 2 variables correspondant au controlleur et à la méthode du controlleur
        $controleur = $this->_controller[0];
        $controleurMethod = $this->_controller[1] ;
        //? print_r($controleurMethod);
        //* instancie un nouvel objet $c selon la valeur de $controleur
        $c = new $controleur();
        //? var_dump($c);
        //* appel la method défini par $controleurMethod avec les bons arguments (vide ou non) pour l'objet $c
        ($c)->$controleurMethod($args);
    }

    //* sanitize() => nettoie les listes $uri et $route en enlevant les éléments qui sont une chaine vide
    
    public function sanitize($array){
        $array2 = [""];
        //* le tableau result contient la différence entre $array et [''] => donc tout ce qui n'est pas du vide!!
        $result = array_diff($array, $array2);
       //? var_dump($result);
        $array3 =[];
        //* envoi chaque valeur de result dans un nouveau tableau pour que les index commencent de 0
        foreach ($result as $key => $value) {
            array_push($array3, $value);
        }
        //* la fonction retourne la tableau sans vide et avec les index commencant de 0
        return $array3;
    }

}
