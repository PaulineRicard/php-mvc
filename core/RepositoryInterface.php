<?php

interface RepositoryInterface
{
    public function getAll();
    public function getAllBy($array); //* $array => tableau associatif pour les clauses WHERE et AND de la base SQL
}