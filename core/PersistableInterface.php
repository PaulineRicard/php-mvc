<?php

interface PersistableInterface 
{
//*METHOD

    //*     //* load(): invoque la méthode retrieve sur le dao en passant en argument l’id de l’entité courante.
    public function load($id);

    //*     //* update(): invoque la méthode create() sur le dao si l’entité courante n’as pas d’id et update dans le cas contraire.
    public function create($id);

    //*     //* remove(): invoque la méthode delete() du dao.
    public function remove($id);
}
