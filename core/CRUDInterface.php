<?php

interface CRUDInterface
{
    public function retrieve($id); //* retourne une entité
    public function update($id); //* retourne un booléen de la réussite du traitement
    public function delete($id); //* retourne un booléen de la réussite du traitement
    public function create($array); //* $array => tableau associatif de propriétés & retourne une entité
}