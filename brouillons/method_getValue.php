<?php

// La méthode getValue($value):
//La méthode retourne la chaine correspondant au contrôleur dont la route est en cours de  test.  Soit  c’est  juste  la  valeur  correspondant  à  la route  soit  c’est  un  tableau associatif dont la clé correspond à la valeur de la super globale REQUEST_METHOD

function getValue($value)
{
    //* $value : correspond au premier étage du tableau
    if (gettype($value[0]) === 'string') :
        return $value[0];
    else :
        foreach ($value as $key => $value1) {
            if ($key === $_SERVER['REQUEST_METHOD']) :
                return $value[0];
            endif;
        }
    endif;
}
